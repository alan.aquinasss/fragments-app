## Task Description
In the scope of this task, you should create an application that will contain one screen (activity) with three sections. Each section (fragment) can communicate with another one. This application will help you understand how fragments can share information with each other and, during development, you will solve the problem of losing data after configuration changes.

## Complete the Task
Create an activity that contains 3 fragments. You need to take the following steps:

1. Fragment1 contains two buttons:

    A button that changes the background color of Fragment2 and Fragment3. You can use random colors or use the background color of Fragment2 for Fragment3 and vice versa. It's up to you.

    A button that swaps Fragment2 to Fragment3 and vice versa.

2. Fragment2 and Fragment3 have an identifying fragment element (ex: a label with the text "Fragment 2" and "Fragment 3").
3. Add the option of saving the state of fragments after rotation (fragments' positions and background colors).