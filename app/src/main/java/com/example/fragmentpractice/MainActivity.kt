package com.example.fragmentpractice

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.os.PersistableBundle
import android.util.Log
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.commit
import com.example.fragmentpractice.R
import com.example.fragmentpractice.fragment.FragmentOne
import com.example.fragmentpractice.fragment.FragmentThree
import com.example.fragmentpractice.fragment.FragmentTwo
import kotlin.random.Random

class MainActivity: AppCompatActivity(), Navigator{
    private lateinit var fragment2: FragmentTwo
    private lateinit var fragment3: FragmentThree

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        if (savedInstanceState == null){
            fragment2 = FragmentTwo.newInstance(Color.WHITE)
            fragment3 = FragmentThree.newInstance(Color.WHITE)
        } else {
            val fragment2Color = savedInstanceState.getInt(COLOR_FRAGMENT_2_KEY)
            val fragment3Color = savedInstanceState.getInt(COLOR_FRAGMENT_3_KEY)
            fragment2 = FragmentTwo.newInstance(fragment2Color)
            fragment3 = FragmentThree.newInstance(fragment3Color)
        }


        supportFragmentManager.beginTransaction()
            .add(R.id.fragment_one, FragmentOne())
            .add(R.id.fragment_two, fragment2)
            .add(R.id.fragment_three, fragment3)
            .commit()

        Log.d(TAG, "onCreate savedInstanceState: ${savedInstanceState != null}")
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        val fragment2Color = (fragment2.view?.background as ColorDrawable).color
        val fragment3Color = (fragment3.view?.background as ColorDrawable).color

        outState.putInt(COLOR_FRAGMENT_2_KEY, fragment2Color)
        outState.putInt(COLOR_FRAGMENT_3_KEY, fragment3Color)

        Log.d(TAG, "onSaveInstanceState")
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        super.onRestoreInstanceState(savedInstanceState)

        Log.d(TAG, "onRestoreInstanceState")
    }

    override fun changeColor() {
//        fragment2.changeBackgroundColor(generateColor())
//        fragment3.changeBackgroundColor(generateColor())
        fragment2.view?.setBackgroundColor(generateColor())
        fragment3.view?.setBackgroundColor(generateColor())
    }

    override fun swapFragments() {
        val fragment2View = fragment2.view
        val fragment3View = fragment3.view

        val parent2 = fragment2View?.parent as ViewGroup
        val parent3 = fragment3View?.parent as ViewGroup

        parent2?.removeView(fragment2View)
        parent3?.removeView(fragment3View)

        parent2?.addView(fragment3View)
        parent3?.addView(fragment2View)
    }



    fun generateColor(): Int {
        return Color.argb(255, Random.nextInt(255), Random.nextInt(255), Random.nextInt(255))
    }

    companion object {
        private const val TAG = "MainActivity"
        private const val COLOR_FRAGMENT_2_KEY = "color_fragment_2_key"
        private const val COLOR_FRAGMENT_3_KEY = "color_fragment_3_key"
    }

}

interface Navigator{
    fun changeColor()
    fun swapFragments()
}