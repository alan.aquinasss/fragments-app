package com.example.fragmentpractice.fragment

import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import com.example.fragmentpractice.R
import com.example.fragmentpractice.databinding.FragmentTwoBinding

class FragmentTwo : Fragment(R.layout.fragment_two) {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return super.onCreateView(inflater, container, savedInstanceState)

    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        Log.d(TAG, "onViewCreated")


        view.setBackgroundColor(arguments?.getInt(ARG_KEY) ?: Color.WHITE)
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.d(TAG, "onDestroy")
    }

    companion object {
        private const val TAG = "FragmentTwo"
        private const val ARG_KEY = "key"
        fun newInstance(color: Int): FragmentTwo = FragmentTwo().apply {
            arguments = bundleOf(
                ARG_KEY to color
            )
        }
    }


}