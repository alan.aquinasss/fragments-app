package com.example.fragmentpractice.fragment

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import androidx.fragment.app.Fragment
import androidx.fragment.app.findFragment
import com.example.fragmentpractice.Navigator
import com.example.fragmentpractice.R

class FragmentOne : Fragment(R.layout.fragment_one) {
    private var navigator: Navigator? = null

    override fun onAttach(context: Context) {
        super.onAttach(context)
        navigator = context as? Navigator
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        Log.d(TAG, "onViewCreated")

        view.findViewById<Button>(R.id.change_color).setOnClickListener {
            navigator?.changeColor()
        }

        view.findViewById<Button>(R.id.swap_fragments).setOnClickListener {
            navigator?.swapFragments()
        }
    }

    override fun onDetach() {
        super.onDetach()
        navigator = null
    }

    companion object {
        const val TAG = "FragmentOne"

        fun newInstance(): FragmentOne {
            return FragmentOne()
        }
    }
}