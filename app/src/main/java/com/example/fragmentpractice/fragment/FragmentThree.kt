package com.example.fragmentpractice.fragment

import android.graphics.Color
import android.os.Bundle
import android.view.View
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import com.example.fragmentpractice.R

class FragmentThree : Fragment(R.layout.fragment_three) {
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        view.setBackgroundColor(arguments?.getInt(FragmentThree.ARG_KEY) ?: Color.WHITE)
    }

    companion object {
        private const val ARG_KEY = "key"
        fun newInstance(color: Int): FragmentThree = FragmentThree().apply {
            arguments = bundleOf(
                ARG_KEY to color
            )
        }
    }
}